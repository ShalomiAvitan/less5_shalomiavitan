#include "Helper.h"
#include <windows.h>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;
typedef int(__stdcall *f_funci)();

//input the string fron the user and remove null space
string inputFromUser()
{
	string input;
	getline(std::cin, input);//insert fron the user
	Helper::trim(input);
	return input;
}

//return the patch current
std::string getExePath()
{
	const unsigned long maxDir = 260;
	char currentDir[maxDir];
	if (!GetCurrentDirectory(maxDir, currentDir))
	{
		cout << "Failed to create process" << endl;
		cout << "Error:" << GetLastError() << endl;
	}
	return string(currentDir);
}

//change the patch to othe patch fron input the user
void changePatch(string patch)
{

	const char* newDir = patch.c_str();//get the patch
	if (!SetCurrentDirectory(newDir)) //changr the patch
	{
			cout << "Failed to create process" << endl;
			cout << "Error:" << GetLastError() << endl;	
	}
}

//creact new file in the folder
//the name file is fron the user
void createFile(string fileName)
{
	std::fstream f;
	f.open(fileName, std::ios::out);//get the name file
	if (!f.is_open())//check if file open
	{
		cout << "Failed to create process" << endl;
		cout << "Error:" << GetLastError() << endl;	
		f.close();
	}
}

//return all files in this folder
std::vector<std::string> GetFileNamesInDirectory(std::string directory)
{
	std::vector<std::string> files;
	WIN32_FIND_DATA fileData;
	HANDLE hFind;

	if (!((hFind = FindFirstFile(directory.c_str(), &fileData)) == INVALID_HANDLE_VALUE)) 
	{
		while (FindNextFile(hFind, &fileData)) {
			files.push_back(fileData.cFileName);
		}
	}
	else//no finde the name files 
	{
		cout << "Failed to create process" << endl;
		cout << "Error:" << GetLastError() << endl;
	}

	FindClose(hFind);
	return files;
}


// the function load the dll file and load the function in the secret.dll file and print what have in this function
void loadDll()
{
	HINSTANCE hGetProcIDDLL = LoadLibrary(R"(C:\Users\magshimim\Desktop\s\SECRET.dll)");//the location dll dile

	if (!hGetProcIDDLL) 
	{
		cout << "Failed to create process" << endl; // dont open
		cout << "Error:" << GetLastError() << endl;
	}

	// find the function and get the value
	f_funci funci = (f_funci)GetProcAddress(hGetProcIDDLL, "TheAnswerToLifeTheUniverseAndEverything");
	if (!funci)
	{
		cout << "Failed to create process" << endl; // dont find the function
		cout << "Error:" << GetLastError() << endl;
	}

	std::cout << "The function return " << funci() << std::endl;
}

void main()
{



	bool check = true;
	while (check)//if not exit
	{
		cout << ">>";
		std::vector<std::string> arrStr;
		string input;
		input = inputFromUser();
		arrStr = Helper::get_words(input); 
		if (arrStr[0] == "pwd")
		{
			cout << getExePath() << endl;
		}
		if (arrStr[0] == "cd")
		{
			changePatch(arrStr[1]);
		}
		if (arrStr[0] == "create")
		{
			createFile(arrStr[1]);
		}
		if (arrStr[0] == "ls")
		{
			std::vector<std::string> vFileNames = GetFileNamesInDirectory("*");
			for (int i = 0; i < vFileNames.size(); i++)
			{
				std::cout << vFileNames[i] << std::endl; //print the all names files in the folder
			}
		}
		if (arrStr[0] == "secret")
		{
			loadDll();
		}
		if (arrStr[0] == "exit")
		{
			check = false;
		}

	}

	system("pause");

}


